from tkinter import *
import math
import decimal
import sqlite3

conexion = sqlite3.connect("BD_Movimiento.db")
cursor = conexion.cursor()

def crear_base_datos():
	cursor.execute("CREATE TABLE IF NOT EXISTS Datos (PosX INTEGER, PosY INTEGER, PosZ INTEGER, Distancia INTEGER, Color TEXT)")
	conexion.commit()

def actualizar_base_datos():
	pj = (p.posX, p.posY, p.posZ, p.distancia(), saber_color())
	cursor.execute("INSERT INTO Datos VALUES (?,?,?,?,?)", pj)
	conexion.commit()

def cerrar_base_datos():
	cursor.close()
	conexion.close()

def saber_color():
	return "#%02x%02x%02x" % (p.cx, p.cy, p.cz)

class Punto:

	def norte(self):
		self.incrementarX()

	def noreste(self):
		self.incrementarX()
		self.incrementarY()

	def este(self):
		self.incrementarY()

	def sureste(self):
		self.incrementarY()
		self.disminuirX()

	def sur(self):
		self.disminuirX()

	def suroeste(self):
		self.disminuirX()
		self.disminuirY()

	def oeste(self):
		self.disminuirY()

	def noroeste(self):
		self.incrementarX()
		self.disminuirY()

	def subir(self):
		self.incrementarZ()

	def bajar(self):
		self.disminuirZ()

	def incrementarX(self):
		self.posX+=1
		self.cx+=self.incrementoColor
		posicion()

	def incrementarY(self):
		self.posY+=1
		self.cy+=self.incrementoColor
		posicion()

	def incrementarZ(self):
		self.posZ+=1
		self.cz+=self.incrementoColor
		posicion()

	def disminuirX(self):
		self.posX-=1
		self.cx-=self.incrementoColor
		posicion()

	def disminuirY(self):
		self.posY-=1
		self.cy-=self.incrementoColor
		posicion()

	def disminuirZ(self):
		self.posZ-=1
		self.cz-=self.incrementoColor
		posicion()

	def distancia(self):
		d = math.sqrt((self.posX)**2+(self.posY)**2+(self.posZ)**2)
		d = abs(d)
		return round(d, 2)

	def __str__(self):
		return "({},{},{})".format(self.posX, self.posY, self.posZ)

	def __init__(self):
		self.posX = 0
		self.posY = 0
		self.posZ = 0 
		self.cx = 120
		self.cy = 120
		self.cz = 120
		self.incrementoColor=10		

def posicion():
	Label(root, text="({},{},{})".format(p.posX, p.posY, p.posZ)).grid(row=3,column=4)
	Button(root, text="Guardar", command=actualizar_base_datos).grid(row=5, column=3, columnspan=3)
	Label(root, text="Distancia: {}m".format(p.distancia())).grid(row=6, column=3, columnspan=3)
	color(p.cx,p.cy,p.cz)

def color(cxc, cyc, czc):

	if(czc>=235 and cxc>=235 and cyc>=235): #Arriba, +X +Y
		cxc =235
		cyc =235
		czc =235
	elif(czc>=235 and cxc<=15 and cyc<=15): #Arriba, -Y -Y
		cxc =15
		cyc =15
		czc =235
	elif(czc>=235 and cxc>=235 and cyc<=15): #Arriba, +X -Y
		cxc =235
		cyc =15
		czc =235
	elif(czc>=235 and cxc<=15 and cyc>=235 ): #Arriba, -Y +X
		cxc =15
		cyc =235
		czc =235
	elif(czc<=15 and cxc>=235 and cyc>=235): #Abajo, +X +Y
		cxc =235
		cyc =235
		czc =15
	elif(czc<=15 and cxc<=15 and cyc<=15): #Abajo, -Y -Y
		cxc =15
		cyc =15
		czc =15
	elif(czc<=15 and cxc>=235 and cyc<=15): #Abajo, +X -Y
		cxc =235
		cyc =15
		czc =15
	elif(czc<=15 and cxc<=15 and cyc>=235 ): #Abajo, -Y +X
		cxc =15
		cyc =235
		czc =15
	elif(czc>=235 and cxc>=235): #Arriba, +X
		cxc =235
		czc =235
	elif(czc>=235 and cxc<=15): #Arriba, -X
		cxc =15
		czc =235
	elif(czc>=235 and cyc>=235): #Arriba, +Y
		czc =235
		cyc =235
	elif(czc>=235 and cyc<=15): #Arriba, -Y
		czc =235
		cyc =15
	elif(czc<=15 and cxc>=235): #Abajo, +X
		cxc =235
		czc =15
	elif(czc<=15 and cxc<=15): #Abajo, -X
		cxc =15
		czc =15
	elif(czc<=15 and cyc>=235): #Abajo, +Y
		czc =15
		cyc =235
	elif(czc<=15 and cyc<=15): #Abajo, -Y
		czc =15
		cyc =15
	elif(cxc>=235 and cyc>=235):
		cxc = 235
		cyc = 235
	elif(cxc<=15 and cyc<=15):
		cxc = 15
		cyc = 15
	elif(cxc>=235 and cyc<=15):
		cxc = 235
		cyc = 15
	elif(cxc<=15 and cyc>=235):
		cxc = 15
		cyc = 235
	elif(cxc>=235):
		cxc = 235
	elif(cyc>=235):
		cyc = 235
	elif(czc>=235):
		czc = 235
	elif(cxc<=15):
		cxc = 15
	elif(cyc<=15):
		cyc = 15
	elif(czc<=15):
		czc = 15

	a = "#%02x%02x%02x" % (cxc, cyc, czc)
	Button(root,width=8,bg=a).grid(row=1,column=1)
	Button(root,width=8,bg=a).grid(row=1,column=7)
	Button(root,width=8,bg=a).grid(row=7,column=1)
	Button(root,width=8,bg=a).grid(row=7,column=7)
	Button(root,width=36,bg=a).grid(row=1,column=2, columnspan=5)
	Button(root,width=8,height=8,bg=a).grid(row=2,column=1, rowspan=5)
	Button(root,width=8,height=8,bg=a).grid(row=2,column=7, rowspan=5)
	Button(root,width=36,bg=a).grid(row=7,column=2, columnspan=5)

root = Tk()
p = Punto()
posicion()

crear_base_datos()

largo = 4
ancho = 6

root.title("Holi")
root.resizable(0,0) 

Button(root, text="ASC",height=4, command=p.subir).grid(row=2,column=2,rowspan=3)
Button(root, text="DES",height=4, command=p.bajar).grid(row=2,column=6,rowspan=3)
Button(root, text="NW",width=8, command=p.noroeste).grid(row=2,column=3)
Button(root, text="N ",width=8, command=p.norte).grid(row=2,column=4)
Button(root, text="NE",width=8, command=p.noreste).grid(row=2,column=5)
Button(root, text="E ",width=8, command=p.este).grid(row=3,column=5)
Button(root, text="SE",width=8, command=p.sureste).grid(row=4,column=5)
Button(root, text="S  ",width=8, command=p.sur).grid(row=4,column=4)
Button(root, text="SW ",width=8, command=p.suroeste).grid(row=4,column=3)
Button(root, text="W  ",width=8, command=p.oeste).grid(row=3,column=3)

#Abajo del todo
root.mainloop()

cerrar_base_datos()
#Añadir 2 botones para recordar 2 puntos que se calcule la distancia
#En otra ventna se guardaa el punto mas lejan en el que hallas estadz